namespace BksTest2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ArticleCommonTables",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ArticleTitle = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ArticleFirstTypeTable",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ArticleFirstTypeProperty = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleCommonTables", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.ArticleSecondTypeTable",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ArticleFirstTypeProperty = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ArticleCommonTables", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ArticleSecondTypeTable", "Id", "dbo.ArticleCommonTables");
            DropForeignKey("dbo.ArticleFirstTypeTable", "Id", "dbo.ArticleCommonTables");
            DropIndex("dbo.ArticleSecondTypeTable", new[] { "Id" });
            DropIndex("dbo.ArticleFirstTypeTable", new[] { "Id" });
            DropTable("dbo.ArticleSecondTypeTable");
            DropTable("dbo.ArticleFirstTypeTable");
            DropTable("dbo.ArticleCommonTables");
        }
    }
}
