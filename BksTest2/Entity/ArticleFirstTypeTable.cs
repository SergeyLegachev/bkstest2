﻿using BksTest2.Entity.Base;

namespace BksTest2.Entity
{
    public class ArticleFirstTypeTable : ArticleCommonTable
    {
        public string ArticleFirstTypeProperty { get; set; }
    }
}
