﻿using System;
using System.Collections.Generic;

namespace BksTest2.Entity.Base
{
    public abstract class Entity : IEntity
    {
        public Guid Id { get; protected set; }

        protected Entity()
        {
            SetId(Guid.NewGuid());
        }
        public void SetId(Guid id)
        {
            Id = id;
        }

        public bool Equals(IEntity obj)
        {
            return obj is Entity identity && Id.Equals(identity.Id);
        }
        public override int GetHashCode()
        {
            return 2108858624 + EqualityComparer<Guid>.Default.GetHashCode(Id);
        }
    }
}
