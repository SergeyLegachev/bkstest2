﻿using System;

namespace BksTest2.Entity.Base
{
    public interface IEntity : IEquatable<IEntity>
    {
        Guid Id { get; }
    }
}
