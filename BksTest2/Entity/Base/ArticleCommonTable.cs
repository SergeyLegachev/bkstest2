﻿namespace BksTest2.Entity.Base
{
    public abstract class ArticleCommonTable : Base.Entity
    {
        public string ArticleTitle { get; set; }
        public ArticleType Type { get; set; }
    }
}
