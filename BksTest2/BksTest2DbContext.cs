﻿using System.Data.Entity;
using BksTest2.Entity;
using BksTest2.Entity.Base;

namespace BksTest2
{
    public class BksTest2DbContext : DbContext
    {
        public BksTest2DbContext() : base("name=Test2Connection")
        {

        }
        public DbSet<ArticleCommonTable> ArticleCommonTables { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ArticleFirstTypeTable>().ToTable("ArticleFirstTypeTable");
            modelBuilder.Entity<ArticleSecondTypeTable>().ToTable("ArticleSecondTypeTable");
        }
    }
}
